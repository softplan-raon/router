import re
from rest_framework.status import is_client_error, is_success
from django.http import HttpResponse

class ResponseFormattingMiddleware:
    """
    Rest Framework 을 위한 전용 커스텀 미들웨어에 대해 response format 을 자동으로 세팅
    """
    METHOD = ('GET', 'POST')

    def __init__(self, get_response):
        self.get_response = get_response
        self.API_URLS = [
            re.compile(r'^(.*)/api'),
            re.compile(r'^api'),
        ]

    def __call__(self, request):
        response = None
        if not response:
            response = self.get_response(request)
        if hasattr(self, 'process_response'):
            response = self.process_response(request, response)
        return response

    def process_view(self, request, view_func, view_args, view_kwargs):


        print("pass reqeust!!")
        print(request.method)
        csrftoken = request.COOKIES.get('csrftoken')
        if csrftoken is None:
            csrftoken = "alkntkeiioncacooniqtaatsqbdfa"
        if csrftoken:
            request.META['HTTP_X_CSRFTOKEN'] = csrftoken
            print("request!!!!")
        response = None
        return response


    def process_response(self, request, response):
        """
        API_URLS 와 method 가 확인이 되면
        response 로 들어온 data 형식에 맞추어
        response_format 에 넣어준 후 response 반환
        """

        response["Server"] = "WSGIServer"
        if request.method in self.METHOD:
            try:
                token = request.META['HTTP_X_CSRFTOKEN']
            except:
                token = None
            if token is None:
                response.content = "{'msg': '올바르지 않은 요청입니다.'}"
                print("here22")
            print("here!!!!")
        else:
            print("option")
            print(response.content)
            response["Allow"] = "GET, HEAD"
            response.content = "{'msg': '올바르지 않은 요청입니다.'}"
            response.data = "{'msg': '올바르지 않은 요청입니다.'}"
            response.body = "{'msg': '올바르지 않은 요청입니다.'}"
            msg = "The request is not valid."
            response = HttpResponse(msg, status=403)
            response["Server"] = "WSGIServer"
            return response

        return response

# class CsrfCookieToHeader(object):
#     def process_view(self, request):
