from rest_framework.permissions import DjangoObjectPermissions

OPTIONS_METHOD = 'OPTIONS'

class DjangoObjectPermissionsOrOptions(DjangoObjectPermissions):
    def has_permission(self, request, view):
        if request.method == OPTIONS_METHOD:
            return True
        else:
            return super(DjangoObjectPermissions, self).has_permission(request, view)