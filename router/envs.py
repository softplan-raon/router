import json
import jinja2

from django.conf import settings
from django.contrib.staticfiles.storage import staticfiles_storage
from django.urls import reverse


def environment(**options):
    env = jinja2.Environment(**options)

    env.globals.update({
        'url': reverse,
        'json': json,
        'debug': settings.DEBUG,
        'round': round,
        'static': staticfiles_storage.url,
        'enumerate': enumerate,
    })

    return env
