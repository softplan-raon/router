from router.settings.settings import *


DEBUG = False

ALLOWED_HOSTS = [
    '*',
]


# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': os.environ.get('ROUTER_DATABASES_DEFAULT_NAME', ''),
        'USER': os.environ.get('ROUTER_DATABASES_DEFAULT_USER', ''),
        'PASSWORD': os.environ.get('ROUTER_DATABASES_DEFAULT_PASSWORD', ''),
        'HOST': os.environ.get('ROUTER_DATABASES_DEFAULT_HOST', ''),
        'PORT': os.environ.get('ROUTER_DATABASES_DEFAULT_PORT', ''),
        'OPTIONS': {
            'charset': 'utf8mb4'
        },
    },
}

REST_FRAMEWORK = {
    'DEFAULT_METADATA_CLASS': None,
}


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.0/howto/static-files/

MEDIA_URL = 'https://s3.ap-northeast-2.amazonaws.com/router-media/'

STATIC_URL = 'https://s3.ap-northeast-2.amazonaws.com/router-static/'

DEFAULT_FILE_STORAGE = 'django_s3_storage.storage.S3Storage'

STATICFILES_STORAGE = 'django_s3_storage.storage.StaticS3Storage'


# https://github.com/etianen/django-s3-storage#authentication-settings

AWS_REGION = os.environ.get('ROUTER_REAL_AWS_REGION', '')

AWS_ACCESS_KEY_ID = os.environ.get('ROUTER_REAL_AWS_ACCESS_KEY_ID', '')

AWS_SECRET_ACCESS_KEY = os.environ.get('ROUTER_REAL_AWS_SECRET_ACCESS_KEY', '')


# https://github.com/etianen/django-s3-storage#file-storage-settings

AWS_S3_BUCKET_NAME = 'router-media'

AWS_S3_BUCKET_AUTH = False

AWS_S3_MAX_AGE_SECONDS = 60 * 60 * 24 * 365


# https://github.com/etianen/django-s3-storage#staticfiles-storage-settings

AWS_S3_BUCKET_NAME_STATIC = 'router-static'

AWS_S3_BUCKET_AUTH_STATIC = False

AWS_S3_MAX_AGE_SECONDS_CACHED_STATIC = 60 * 60 * 24 * 265
