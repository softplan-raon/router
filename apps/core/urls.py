from django.urls import path

from apps.core.views import *


urlpatterns = [
    path('', ReqView.as_view(), name='req'),
    path('index', ReqView.as_view(), name='req'),
    path('res_1/index', ReqView.as_view(), name='req'),
    path('res_2/index', ReqView.as_view(), name='req'),
    path('res_1/', Res1View.as_view(), name='res_1'),
    path('res_2/', Res2View.as_view(), name='res_2'),
]
