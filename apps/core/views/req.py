from django.views import generic


class ReqView(generic.TemplateView):
    template_name = 'core/req.html'
