import pprint

from django.views import generic
from django.contrib import messages

from apps.core.models import Node


class ResView(generic.TemplateView):
    init_node = None

    shop_nodes = []
    visited_shop_nodes = []
    not_visited_shop_nodes = []

    pass_nodes = []
    visited_pass_nodes = []
    not_visited_pass_nodes = []

    shop_nodes_limit_number = 0
    pass_nodes_limit_number = 0
    shop_nodes_limit_minute = 0
    pass_nodes_limit_minute = 0

    cus_messages = []

    def dispatch(self, request, *args, **kwargs):

        self.cus_messages = []
        if self.request.GET.get('init_node') == "":
            self.cus_messages.append('기사 위치 값을 입력해주세요.')

        if self.request.GET.get('shop_nodes') == "":
            self.cus_messages.append('매장 위치 값들을 입력해주세요.')

        if self.request.GET.get('pass_nodes') == "":
            self.cus_messages.append('배달목적지 위치 값들을 입력해주세요.')

        # init_node 설정
        self.init_node = Node.objects.create(
            address=self.request.GET['init_node'],
        )
        # shop_nodes 설정
        self.shop_nodes = []

        for data in self.request.GET['shop_nodes'].split('}{'):
            if data:
                address = data.split(')(')[0]
                seq = data.split(')(')[1]
                self.shop_nodes.append(
                    Node.objects.create(
                        seq=seq,
                        address=address,
                    )
                )

        # pass_nodes 설정
        self.pass_nodes = []

        for data in self.request.GET['pass_nodes'].split('}{'):
            if data:
                address = data.split(')(')[0]
                seq = data.split(')(')[1]
                related_shop_node = [
                    shop_node for shop_node in self.shop_nodes
                    if shop_node.seq == data.split(')(')[1]
                ][0]

                self.pass_nodes.append(
                    Node.objects.create(
                        seq=seq,
                        address=address,
                        related_shop_node=related_shop_node,
                    )
                )


        # shop_nodes_limit_number 설정
        self.shop_nodes_limit_number = self.request.GET.get('limit_shop_nodes_number')

        if self.shop_nodes_limit_number:
            self.shop_nodes_limit_number = int(self.shop_nodes_limit_number)

        else:
            self.shop_nodes_limit_number = 99999999

        # pass_nodes_limit_number 설정
        self.pass_nodes_limit_number = self.request.GET.get('limit_pass_nodes_number')

        if self.pass_nodes_limit_number:
            self.pass_nodes_limit_number = int(self.pass_nodes_limit_number)

        else:
            self.pass_nodes_limit_number = 99999999

        # shop_nodes_limit_minute 설정
        self.shop_nodes_limit_minute = self.request.GET.get('limit_shop_nodes_minute')

        if self.shop_nodes_limit_minute:
            self.shop_nodes_limit_minute = int(self.shop_nodes_limit_minute)

        else:
            self.shop_nodes_limit_minute = 99999999

        # pass_nodes_limit_minute 설정
        self.pass_nodes_limit_minute = self.request.GET.get('limit_pass_nodes_minute')

        if self.pass_nodes_limit_minute:
            self.pass_nodes_limit_minute = int(self.pass_nodes_limit_minute)

        else:
            self.pass_nodes_limit_minute = 99999999

        return super().dispatch(request, *args, **kwargs)


class Res1View(ResView):
    shop_nodes_routes = []
    pass_nodes_routes = []
    template_name = 'core/res_1.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # shop node 순서 설정
        # self.shop_nodes_limit_number, self.shop_nodes_limit_minute 값과 관련 있음

        # shop node 관련 제한 설정
        shop_nodes_limit_number = 0
        shop_nodes_limit_minute = 0

        # shop node 관련 초기화 진행
        self.shop_nodes_routes = []
        self.visited_shop_nodes = []
        self.not_visited_shop_nodes = []

        _shop_nodes = sorted(self.shop_nodes[:], key=lambda n: n.distance(self.init_node))

        for i in range(min(len(_shop_nodes), self.shop_nodes_limit_number)):
            shop_node = _shop_nodes.pop()

            init_node = self.visited_shop_nodes[-1] if self.visited_shop_nodes else self.init_node

            route = {
                'init_node': init_node,
                'term_node': shop_node,
                'nodes': '',
                'data': Node.request_tmap_routes(
                    init_node=init_node,
                    term_node=shop_node,
                ),
            }

            shop_nodes_limit_number += 1
            shop_nodes_limit_minute += route['data']['total_time']

            if shop_nodes_limit_number <= self.shop_nodes_limit_number and \
               shop_nodes_limit_minute <= self.shop_nodes_limit_minute:

                self.shop_nodes_routes.append(route)
                self.visited_shop_nodes.append(shop_node)

                # 다시 정렬
                _shop_nodes.sort(key=lambda n: n.distance(self.visited_shop_nodes[-1]))

                continue

            else:
                shop_nodes_limit_number -= 1
                shop_nodes_limit_minute -= route['data']['total_time']
                _shop_nodes.append(shop_node)
                # _shop_nodes.sort(key=lambda n: n.distance(self.visited_shop_nodes[-1]))

                break

        self.not_visited_shop_nodes = _shop_nodes[:]

        # pass node 순서 설정
        # self.pass_nodes_limit_number, self.pass_nodes_limit_minute 값과 관련 있음

        # pass node 관련 제한 설정
        pass_nodes_limit_number = 0
        pass_nodes_limit_minute = 0

        # pass node 관련 초기화 진행
        self.pass_nodes_routes = []
        self.visited_pass_nodes = []
        self.not_visited_pass_nodes = []

        _pass_nodes = sorted(self.pass_nodes[:], key=lambda n: n.distance(self.init_node))

        for i in range(min(len(_pass_nodes), self.pass_nodes_limit_number)):
            pass_node = _pass_nodes.pop()

            if self.visited_pass_nodes:
                init_node = self.visited_pass_nodes[-1]
            elif self.visited_shop_nodes:
                init_node = self.visited_shop_nodes[-1]
            else:
                break

            seq_not_visited_shop_nodes = [x.seq for x in self.not_visited_shop_nodes]
            if pass_node.seq in seq_not_visited_shop_nodes:
                continue

            route = {
                'init_node': init_node,
                'term_node': pass_node,
                'nodes': '',
                'data': Node.request_tmap_routes(
                    init_node=init_node,
                    term_node=pass_node,
                ),
            }

            if self.pass_nodes_limit_number >= pass_nodes_limit_number + 1 and \
               self.pass_nodes_limit_minute >= pass_nodes_limit_minute + route['data']['total_time']:

                self.pass_nodes_routes.append(route)
                self.visited_pass_nodes.append(pass_node)

                pass_nodes_limit_number += 1
                pass_nodes_limit_minute += route['data']['total_time']

                # 다시 정렬
                _pass_nodes.sort(key=lambda n: n.distance(self.visited_pass_nodes[-1]))

                continue

            else:
                break

        self.not_visited_pass_nodes = _pass_nodes[:]

        shop_nodes_routes_total_dist = sum([route['data']['total_dist'] for route in self.shop_nodes_routes])
        shop_nodes_routes_total_time = sum([route['data']['total_time'] for route in self.shop_nodes_routes])

        pass_nodes_routes_total_dist = sum([route['data']['total_dist'] for route in self.pass_nodes_routes])
        pass_nodes_routes_total_time = sum([route['data']['total_time'] for route in self.pass_nodes_routes])

        context.update({
            'init_node': self.init_node,
            # shop node 관련
            'shop_nodes': self.shop_nodes,
            'shop_nodes_routes': self.shop_nodes_routes,
            'visited_shop_nodes': self.visited_shop_nodes,
            # pass node 관련
            'pass_nodes': self.pass_nodes,
            'pass_nodes_routes': self.pass_nodes_routes,
            'visited_pass_nodes': self.visited_pass_nodes,
            'not_visited_shop_nodes': self.not_visited_shop_nodes,
            # routes 관련
            'routes_total_dist': round(shop_nodes_routes_total_dist + pass_nodes_routes_total_dist),
            'routes_total_time': round(shop_nodes_routes_total_time + pass_nodes_routes_total_time),
            'cus_messages': self.cus_messages,
            'not_visited_number': self.not_visited_shop_nodes.__len__(),
        })
        if self.shop_nodes_limit_minute != 99999999:
            context.update({
                'shop_nodes_limit_minute': self.shop_nodes_limit_minute,
            })
        if self.shop_nodes_limit_number != 99999999:
            context.update({
                'shop_nodes_limit_number': self.shop_nodes_limit_number,
            })
        if self.pass_nodes_limit_minute != 99999999:
            context.update({
                'pass_nodes_limit_minute': self.pass_nodes_limit_minute,
            })
        if self.pass_nodes_limit_number != 99999999:
            context.update({
                'pass_nodes_limit_number': self.pass_nodes_limit_number,
            })
        if self.request.GET.get('speed'):
            context.update({
                'speed': self.request.GET['speed'],
                'routes_total_time': round((context['routes_total_dist'] / float(self.request.GET['speed'])) * 60, 2),
            })

        return context


class Res2View(ResView):
    routes = []

    template_name = 'core/res_2.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)


        # 관련 제한 설정
        limit_minute = 0
        shop_nodes_limit_number = 0
        pass_nodes_limit_number = 0

        # 관련 초기화 진행
        self.routes = []

        self.visited_shop_nodes = []
        self.not_visited_shop_nodes = []

        self.visited_pass_nodes = []
        self.not_visited_pass_nodes = []

        _shop_nodes = sorted(self.shop_nodes[:], key=lambda n: n.distance(self.init_node))
        _pass_nodes = self.pass_nodes

        for i in range(min(len(_shop_nodes), self.shop_nodes_limit_number)):
            shop_node = _shop_nodes.pop()

            init_node = self.visited_pass_nodes[-1] if self.visited_pass_nodes else self.init_node

            route = {
                'init_node': init_node,
                'term_node': shop_node,
                'nodes': '',
                'data': Node.request_tmap_routes(
                    init_node=init_node,
                    term_node=shop_node,
                ),
            }

            if self.shop_nodes_limit_number >= shop_nodes_limit_number + 1 and \
               self.shop_nodes_limit_minute >= limit_minute + route['data']['total_time']:

                self.routes.append(route)
                self.visited_shop_nodes.append(shop_node)

                shop_nodes_limit_number += 1
                limit_minute += route['data']['total_time']

                # 다시 정렬
                _shop_nodes.sort(key=lambda n: n.distance(self.visited_shop_nodes[-1]))

                _pass_nodes = [
                    pass_node for pass_node in self.pass_nodes
                    if pass_node.related_shop_node == self.visited_shop_nodes[-1]
                ]

                for j in range(min(len(_pass_nodes), self.pass_nodes_limit_number)):
                    pass_node = _pass_nodes.pop()

                    routes = [
                        {
                            'init_node': shop_node,
                            'term_node': pass_node,
                            'nodes': '',
                            'data': Node.request_tmap_routes(
                                init_node=shop_node,
                                term_node=pass_node,
                            ),
                        },
                    ]

                    # if self.pass_nodes_limit_number >= pass_nodes_limit_number + 1 and \
                    #    self.shop_nodes_limit_minute >= limit_minute + sum([route['data']['total_time'] for route in routes]):
                    self.routes += routes
                    self.visited_pass_nodes.append(pass_node)

                    pass_nodes_limit_number += 1
                    limit_minute += sum([route['data']['total_time'] for route in routes])

                    # else:
                    #
                    #     break
                # 처음에 pop 하고 다시 안넣고 있음.
                continue

            else:
                _shop_nodes.append(shop_node)
                break

        self.not_visited_shop_nodes = _shop_nodes[:]
        self.not_visited_pass_nodes = _pass_nodes[:]

        context.update({
            'init_node': self.init_node,
            # shop node 관련
            'shop_nodes': self.shop_nodes,
            'visited_shop_nodes': self.visited_shop_nodes,
            # pass node 관련
            'pass_nodes': self.pass_nodes,
            'visited_pass_nodes': self.visited_pass_nodes,
            'not_visited_shop_nodes': self.not_visited_shop_nodes,
            # routes 관련
            'routes': self.routes,
            'routes_total_dist': round(sum([route['data']['total_dist'] for route in self.routes])),
            'routes_total_time': round(sum([route['data']['total_time'] for route in self.routes])),
            'cus_messages': self.cus_messages,
            'not_visited_number': 0 if(_shop_nodes.__len__() == 0) else _shop_nodes.__len__(),
        })

        if self.shop_nodes_limit_minute != 99999999:
            context.update({
                'shop_nodes_limit_minute': self.shop_nodes_limit_minute,
            })
        if self.shop_nodes_limit_number != 99999999:
            context.update({
                'shop_nodes_limit_number': self.shop_nodes_limit_number,
            })
        if self.pass_nodes_limit_minute != 99999999:
            context.update({
                'pass_nodes_limit_minute': self.pass_nodes_limit_minute,
            })
        if self.pass_nodes_limit_number != 99999999:
            context.update({
                'pass_nodes_limit_number': self.pass_nodes_limit_number,
            })

        if self.request.GET.get('speed'):
            context.update({
                'speed': self.request.GET['speed'],
                'routes_total_time': round((context['routes_total_dist'] / float(self.request.GET['speed'])) * 60, 2),
            })

        return context
