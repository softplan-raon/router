import math
import pprint
import requests
import datetime

from django.db import models
from django.utils.translation import gettext_lazy as _

from router.classes.db.models import MetaDataModel


class Node(MetaDataModel):
    class Meta:
        verbose_name = _('장소')
        verbose_name_plural = _('장소 목록')

    lat = models.FloatField(
        null=True,
        default=None,
        verbose_name='위도',
    )
    lng = models.FloatField(
        null=True,
        default=None,
        verbose_name='경도',
    )
    seq = models.PositiveIntegerField(
        null=True,
        default=None,
        verbose_name='순번',
    )
    address = models.CharField(
        blank=True,
        default='',
        max_length=255,
        verbose_name='주소',
    )
    related_shop_node = models.ForeignKey(
        to='core.Node',
        null=True,
        default=None,
        on_delete=models.CASCADE,
        related_name='pass_nodes',
        verbose_name='관련 매장',
    )

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if self.address and (not self.lat and not self.lng):
            response = Node.request_daum_local_search_address(self)

            self.lng, self.lat = response['documents'][0]['x'], response['documents'][0]['y']

        super().save(force_insert, force_update, using, update_fields)

    def distance(self, node):
        return math.sqrt(
            (float(self.lat) - float(node.lat)) ** 2 +
            (float(self.lng) - float(node.lng)) ** 2
        )

    @classmethod
    def request_daum_local_search_address(cls, node):
        response = requests.request(
            url='https://dapi.kakao.com/v2/local/search/address.json',
            method='GET',
            headers={
                'Authorization': f'KakaoAK 9eccda5ed53ef5d943842a9addc7f952',
            },
            params={
                'query': node.address,
            },
        )

        try:
            response.raise_for_status()

        except requests.exceptions.HTTPError as e:
            print(e)

        return response.json()

    @classmethod
    def request_tmap_routes(cls, init_node, term_node):
        data = {
            'angle': '172',
            'searchOption': 0,
            'reqCoordType': 'WGS84GEO',
            'resCoordType': 'EPSG3857',
            'startX': str(init_node.lng),
            'startY': str(init_node.lat),
            'endX': str(term_node.lng),
            'endY': str(term_node.lat),
        }

        response = requests.request(
            url='https://api2.sktelecom.com/tmap/routes?version=1&format=json',
            method='POST',
            headers={
                'appKey': f'f3822d08-6fcc-4d0c-b6bd-fb7634cea861',
            },
            data=data,
        )

        try:
            response.raise_for_status()

        except requests.exceptions.HTTPError as e:
            print(e)
            pprint.pprint(data)
            pprint.pprint(response.json())

        response = response.json()

        total_dist = float(response['features'][0]['properties']['totalDistance'])
        total_dist = total_dist / 1000

        total_time = float(response['features'][0]['properties']['totalTime'])
        total_time = total_time / 60

        return {
            'response': response,
            'total_dist': total_dist,
            'total_time': total_time
        }

    @classmethod
    def request_tmap_routes_route_optimization(cls, init_node, term_node, nodes):
        data = {
            'reqCoordType': 'WGS84GEO',
            'resCoordType': 'EPSG3857',
            'startTime': datetime.datetime.now().strftime('%Y%m%d%H%M'),
            'startName': str(init_node.address),
            'startX': str(init_node.lng),
            'startY': str(init_node.lat),
            'endName': str(term_node.address),
            'endX': str(term_node.lng),
            'endY': str(term_node.lat),
            'viaPoints': [
                {
                    'viaPointId': str(node.id),
                    'viaPointName': str(node.address),
                    'viaX': str(node.lng),
                    'viaY': str(node.lat),
                }
                for node in nodes if (node != init_node) and (node != term_node)
            ],
        }

        if len(nodes) < 10:
            url = 'https://api2.sktelecom.com/tmap/routes/routeOptimization10?version=1&format=json'

        elif len(nodes) < 20:
            url = 'https://api2.sktelecom.com/tmap/routes/routeOptimization20?version=1&format=json'

        elif len(nodes) < 30:
            url = 'https://api2.sktelecom.com/tmap/routes/routeOptimization30?version=1&format=json'

        else:
            raise ValueError

        response = requests.request(
            url=url,
            method='POST',
            headers={
                'appKey': f'f3822d08-6fcc-4d0c-b6bd-fb7634cea861',
            },
            json=data,
        )

        try:
            response.raise_for_status()

        except requests.exceptions.HTTPError as e:
            print(e)
            pprint.pprint(data)
            pprint.pprint(response.json())

        response = response.json()

        total_dist = float(response['properties']['totalDistance'])
        total_dist = total_dist / 1000

        total_time = float(response['properties']['totalTime'])
        total_time = total_time / 60

        return {
            'response': response,
            'total_dist': total_dist,
            'total_time': total_time
        }
